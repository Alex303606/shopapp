import {
	ITEMS_SUCCESS,
	FETCH_ITEM_INFO_SUCCESS,
	CREATE_ITEM_FAILURE,
	CREATE_ITEM_SUCCESS
} from "../actions/actionType";

const initialState = {
	items: [],
	currentItem: null,
	createError: null
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ITEMS_SUCCESS:
			return {...state, items: action.items};
		case FETCH_ITEM_INFO_SUCCESS:
			return {...state, currentItem: action.item};
		case CREATE_ITEM_FAILURE:
			return {...state, createError: action.error};
		case CREATE_ITEM_SUCCESS:
			return {...state, createError: null};
		default:
			return state;
	}
};

export default reducer;