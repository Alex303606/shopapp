import axios from '../../axios-api';
import { GET_CATEGORIES } from "./actionType";

export const getCategoriesList = () => {
	return dispatch => {
		return axios.get('/categories').then(response => {
			dispatch(getCategories(response.data));
		})
	}
};

const getCategories = categories => {
	return {type: GET_CATEGORIES, categories};
};