import axios from '../../axios-api';
import { push } from 'react-router-redux';
import { ITEMS_SUCCESS, FETCH_ITEM_INFO_SUCCESS, CREATE_ITEM_FAILURE, CREATE_ITEM_SUCCESS } from "./actionType";

export const createNewItem = itemData => {
	return (dispatch, getState) => {
		const token = getState().usr.user.token;
		const headers = {'Token': token};
		axios.post('/items', itemData, {headers: headers}).then(
			response => {
				dispatch(createItemSuccess());
				dispatch(push('/'));
			},
			error => {
				dispatch(createItemFailure(error.response.data));
			}
		);
	};
};

const createItemFailure = error => {
	return {type: CREATE_ITEM_FAILURE, error};
};

const createItemSuccess = error => {
	return {type: CREATE_ITEM_SUCCESS, error};
};

export const getAllItems = () => {
	return dispatch => {
		axios.get('/items').then(response => {
			dispatch(itemsSuccess(response.data));
		})
	}
};

const itemsSuccess = items => {
	return {type: ITEMS_SUCCESS, items};
};

export const goToItemPage = id => {
	return dispatch => {
		dispatch(push(`/item${id}`));
	}
};

export const getToItemInfo = id => {
	return dispatch => {
		return axios.get(`/items/${id}`).then(response => {
			dispatch(fetchItemInfoSuccess(response.data));
		});
	}
};

const fetchItemInfoSuccess = item => {
	return {type: FETCH_ITEM_INFO_SUCCESS, item};
};

export const deleteItemFromDb = id => {
	return (dispatch, getState) => {
		const token = getState().usr.user.token;
		const headers = {'Token': token};
		axios.delete('/items/' + id, {headers: headers}).then(
			() => dispatch(push('/'))
		);
	};
};

export const getCategoryItems = id => {
	return dispatch => {
		axios.get(`/items?category=${id}`).then(response => {
			dispatch(itemsSuccess(response.data));
		})
	}
};