import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { routerMiddleware, routerReducer } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import thunkMiddleware from "redux-thunk";
import { loadState, saveState } from "./localStorage";
import userReducer from "./reducers/users";
import categoriesReducer from "./reducers/categories";
import itemsReducer from "./reducers/items";

const rootReducer = combineReducers({
	usr: userReducer,
	categories: categoriesReducer,
	items: itemsReducer,
	routing: routerReducer
});

export const history = createHistory();

const middleware = [
	thunkMiddleware,
	routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
	saveState({
		usr: store.getState().usr
	});
});

export default store;