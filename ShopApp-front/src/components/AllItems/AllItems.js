import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllItems, goToItemPage } from "../../store/actions/items";
import Item from "../Item/Item";
import './AllItems.css';

class AllItems extends Component {
	
	componentDidMount() {
		this.props.getAllItems();
	}
	
	render() {
		return (
			<div className="all_items">
				{
					this.props.items.map(item => (
						<Item
							click={() => this.props.goToItemPage(item._id)}
							image={item.image}
							key={item._id}
							price={item.price}
							title={item.title}
						/>
					))
				}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	items: state.items.items
});

const mapDispatchToProps = dispatch => ({
	getAllItems: () => dispatch(getAllItems()),
	goToItemPage: id => dispatch(goToItemPage(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AllItems);
