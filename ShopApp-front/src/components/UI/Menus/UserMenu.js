import React, { Fragment } from 'react';
import { NavLink } from "react-router-dom";

const UserMenu = ({user, logout}) => {
	return (
		<Fragment>
			<li><NavLink to="/add_item">Add new item</NavLink></li>
			<li><span onClick={logout}>Logout</span></li>
			<li className="username_menu">Hello, <b>{user.username}</b></li>
		</Fragment>
	)
};

export default UserMenu;
