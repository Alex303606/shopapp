import React, { Component, Fragment } from 'react';
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

class AnonymousMenu extends Component {
	
	render() {
		return (
			<Fragment>
				<li><NavLink to="/register" exact>Sign Up</NavLink></li>
				<li><NavLink to="/login">Login</NavLink></li>
			</Fragment>
		)
	}
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(AnonymousMenu);
