import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from "react-router-dom";
import './Header.css';
import UserMenu from "../Menus/UserMenu";
import AnonymousMenu from "../Menus/AnonymousMenu";
import logo from '../../../assets/images/shop_logo_big.png';
import { logoutUser } from "../../../store/actions/users";

class Header extends Component {
	render() {
		return (
			<header>
				<div className="container">
					<NavLink to='/' className="header__logo">
						<img src={logo} alt="logo"/>
					</NavLink>
					<ul>
						{this.props.user ? <UserMenu logout={this.props.logoutUser} user={this.props.user}/> :
							<AnonymousMenu/>}
					</ul>
				</div>
			</header>
		);
	}
}

const mapStateToProps = state => ({
	user: state.usr.user
});

const mapDispatchToProps = dispatch => ({
	logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
