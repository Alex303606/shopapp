import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCategoriesList } from "../../store/actions/categories";
import './Sidebar.css';
import { getAllItems, getCategoryItems } from "../../store/actions/items";

class Sidebar extends Component {
	
	componentDidMount() {
		this.props.getCategoriesList();
	}
	
	render() {
		return (
			<ul className="Sidebar">
				<li onClick={this.props.getAllItems}>All categories</li>
				{
					this.props.categories.map(category => (
						<li onClick={() => this.props.getCategoryItems(category._id)}
						    key={category._id}>{category.title}</li>
					))
				}
			</ul>
		);
	}
}

const mapStateToProps = state => ({
	categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
	getCategoriesList: () => dispatch(getCategoriesList()),
	getAllItems: () => dispatch(getAllItems()),
	getCategoryItems: id => dispatch(getCategoryItems(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
