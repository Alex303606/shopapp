import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteItemFromDb, getToItemInfo } from "../../store/actions/items";
import config from '../../config';
import './ItemPage.css';
import Preloader from "../UI/Preloader/Preloader";

class ItemPage extends Component {
	
	componentDidMount() {
		this.props.getToItemInfo(this.props.match.params.id);
	}
	
	deleteItem = () => {
		this.props.deleteItemFromDb(this.props.item._id);
	};
	
	render() {
		if (!this.props.item) {
			return <Preloader/>;
		}
		return (
			<div className="item_page container">
				<h1>{this.props.item.title}</h1>
				<h3>{this.props.item.categoryId.title}</h3>
				<div className="wrap_item">
					<img src={config.apiUrl + '/uploads/' + this.props.item.image} alt=""/>
					<div className="info">
						<p className="descr">
							Description: {this.props.item.description}
						</p>
						<span className="price">Price: {this.props.item.price} KGZ</span>
						<span className="user">User: {this.props.item.userId.displayName}</span>
						<span className="phone">
							Phone:
							<a href={'tel:' + this.props.item.userId.phone}> {this.props.item.userId.phone}</a>
						</span>
						{(this.props.user && this.props.user._id === this.props.item.userId._id) &&
							<button className="delete_item" onClick={this.deleteItem}>Delete</button>
						}
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	item: state.items.currentItem,
	user: state.usr.user
});

const mapDispatchToProps = dispatch => ({
	getToItemInfo: id => dispatch(getToItemInfo(id)),
	deleteItemFromDb: id => dispatch(deleteItemFromDb(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemPage);
