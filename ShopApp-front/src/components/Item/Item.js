import React from 'react';
import config from '../../config';
import './Item.css';

const Item = props => {
	return (
		<div className="item" onClick={props.click}>
			{props.image &&
			<div style={{
				backgroundImage: 'url(' + config.apiUrl + '/uploads/' + props.image + ')',
				backgroundSize: 'contain',
				backgroundPosition: 'center',
				backgroundRepeat: 'no-repeat'
			}} className="image"/>
			}
			<div className="title">{props.title}</div>
			<div className="price">{props.price} KGS</div>
		</div>
	);
};

export default Item;
