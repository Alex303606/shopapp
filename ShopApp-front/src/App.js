import React, { Component, Fragment } from 'react';
import Header from "./components/UI/Header/Header";
import { Route, Switch } from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddItem from "./containers/AddItem/AddItem";
import Main from "./containers/Main/Main";
import ItemPage from "./components/ItemPage/ItemPage";

class App extends Component {
	render() {
		return (
			<Fragment>
				<Header/>
				<Switch>
					<Route path="/" exact component={Main}/>
					<Route path="/item:id" exact component={ItemPage}/>
					<Route path="/register" exact component={Register}/>
					<Route path="/login" exact component={Login}/>
					<Route path="/add_item" exact component={AddItem}/>
				</Switch>
			</Fragment>
		);
	}
}

export default App;
