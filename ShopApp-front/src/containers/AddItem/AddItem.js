import React, { Component } from 'react';
import { connect } from 'react-redux';
import './AddItem.css';
import { getCategoriesList } from "../../store/actions/categories";
import { createNewItem } from "../../store/actions/items";

class AddItem extends Component {
	
	state = {
		title: '',
		description: '',
		image: '',
		categoryId: '',
		preview: '',
		price: 0
	};
	
	
	componentDidMount() {
		if (this.props.user) {
			this.props.getCategoriesList().then(() => {
				this.setState({categoryId: this.props.categories[0]._id});
			});
		} else {
			this.props.history.push('/login');
		}
	}
	
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if (key !== 'preview') formData.append(key, this.state[key]);
		});
		this.props.createNewItem(formData);
	};
	
	render() {
		
		const errorStyle = {
			borderWidth: '2px',
			borderColor: '#ff0000',
			backgroundColor: '#ffcacf'
		};
		
		return (
			<div className="add_item container">
				<h1>Create new item</h1>
				<form onSubmit={this.submitFormHandler}>
					<div className="row">
						<label htmlFor="title">Title: </label>
						<input style={this.props.createError && errorStyle} value={this.state.title} onChange={this.inputChangeHandler}
						       id="title" name="title"
						       type="text"/>
					</div>
					<div className="row">
						<label htmlFor="description">Description: </label>
						<input style={this.props.createError && errorStyle} value={this.state.description} onChange={this.inputChangeHandler}
						       id="description" name="description"
						       type="text"/>
					</div>
					<div className="row">
						<label htmlFor="price">Price: </label>
						<input style={this.props.createError && errorStyle} value={this.state.price} onChange={this.inputChangeHandler}
						       id="price" name="price"
						       type="number"/>
					</div>
					<div className="row">
						<label style={this.props.createError && errorStyle} className="addImage" htmlFor="image">Add image</label>
						<input id="image"
						       style={{display: 'none'}}
						       name="image"
						       onChange={this.fileChangeHandler}
						       type="file"/>
						{this.state.preview && <img className="previewImg" src={this.state.preview} alt="previewImg"/>}
					</div>
					<div className="row">
						<label htmlFor="category">Select category: </label>
						<select value={this.state.categoryId} onChange={this.inputChangeHandler} name="categoryId"
						        id="category">
							{
								this.props.categories.map(category => (
									<option key={category._id} value={category._id}>{category.title}</option>
								))
							}
						</select>
					</div>
					{
						this.props.createError &&
						<span className="error">Заполните все поля!!!</span>
					}
					<div className="row">
						<button>Create item</button>
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	categories: state.categories.categories,
	user: state.usr.user,
	createError: state.items.createError
});

const mapDispatchToProps = dispatch => ({
	getCategoriesList: () => dispatch(getCategoriesList()),
	createNewItem: itemData => dispatch(createNewItem(itemData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddItem);
