import React, { Component } from 'react';
import { connect } from 'react-redux';
import Sidebar from "../../components/Sidebar/Sidebar";
import AllItems from "../../components/AllItems/AllItems";
import './Main.css';

class Main extends Component {
	render() {
		return (
			<main className="container main_page">
				<Sidebar/>
				<AllItems/>
			</main>
		);
	}
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
