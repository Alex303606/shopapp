import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Login.css';
import { loginUser } from "../../store/actions/users";

class Login extends Component {
	
	state = {
		username: '',
		password: ''
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		this.props.loginUser(this.state);
	};
	
	render() {
		return (
			<div className="register_user container">
				<h1>Sign in</h1>
				<form onSubmit={this.submitFormHandler}>
					<div className="row">
						<label htmlFor="username">Enter username:</label>
						<input value={this.state.username} onChange={this.inputChangeHandler} id="username" name="username" type="text"/>
					</div>
					<div className="row">
						<label htmlFor="password">Enter password:</label>
						<input value={this.state.password} onChange={this.inputChangeHandler} id="password" name="password" type="password"/>
					</div>
					{this.props.loginError && <span className="error">{this.props.loginError.error}</span>}
					<div className="row">
						<button><span>Login</span></button>
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	loginError: state.usr.loginError,
});

const mapDispatchToProps = dispatch => ({
	loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
