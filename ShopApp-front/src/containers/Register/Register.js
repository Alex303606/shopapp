import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Register.css';
import { registerUser } from "../../store/actions/users";

class Register extends Component {
	
	state = {
		username: '',
		password: '',
		displayName: '',
		phone: ''
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		this.props.registerUser(this.state);
	};
	
	render() {
		const errorStyle = {
			borderWidth: '2px',
			borderColor: '#ff0000',
			backgroundColor: '#ffcacf'
		};
		return (
			<div className="register_user container">
				<h1>Register new user</h1>
				<form onSubmit={this.submitFormHandler}>
					<div className="row">
						<label htmlFor="username">Enter username:</label>
						<input style={this.props.registerError && errorStyle} value={this.state.username} onChange={this.inputChangeHandler} id="username"
						       name="username" type="text"/>
					</div>
					<div className="row">
						<label htmlFor="password">Enter password:</label>
						<input style={this.props.registerError && errorStyle} value={this.state.password} onChange={this.inputChangeHandler} id="password"
						       name="password" type="password"/>
					</div>
					<div className="row">
						<label htmlFor="displayName">Enter your name:</label>
						<input style={this.props.registerError && errorStyle} value={this.state.displayName} onChange={this.inputChangeHandler} id="displayName"
						       name="displayName" type="text"/>
					</div>
					<div className="row">
						<label htmlFor="phone">Enter your phone:</label>
						<input style={this.props.registerError && errorStyle} value={this.state.phone} onChange={this.inputChangeHandler} id="phone" name="phone"
						       type="number"/>
					</div>
					{
						this.props.registerError &&
						<span className="error">Заполните все поля!!!</span>
					}
					<div className="row">
						<button><span>Register</span></button>
					</div>
				</form>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	registerUser: userData => dispatch(registerUser(userData))
});

const mapStateToProps = state => ({
	registerError: state.usr.registerError
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
