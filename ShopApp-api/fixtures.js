const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Item = require('./models/Item');
const Users = require('./models/Users');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
	try {
		await db.dropCollection('categories');
		await db.dropCollection('﻿users');
		await db.dropCollection('items');
	} catch (e) {
		console.log('Collections were not present, skipping drop');
	}
	const [clothesCategory, HiTechCategory, furnitureCategory] = await Category.create(
		{title: 'clothes'},
		{title: 'hi-tech'},
		{title: 'furniture'}
	);
	
	const [root, user] = await Users.create(
			{
				username: 'root',
				password: 'root',
				displayName: 'Alex',
				phone: '0555303606',
				token: 'fYPXcXP4MUacTfgBzXkrC'
			},
			{
				username: 'user',
				password: 'user',
				displayName: 'User',
				phone: '﻿0555777666',
				token: '﻿J1bv83TkM4rT9HoQ4F3t1'
			}
		);
	
	await Item.create({
			title: 'Ipad',
			price: 30000,
			description: 'Very cool Ipad',
			categoryId: HiTechCategory._id,
			image: 'ipad.png',
			userId: root._id
		}, {
			title: 'Shirt',
			price: 1100,
			description: 'Some kinda description',
			categoryId: clothesCategory._id,
			image: 'shirt.jpeg',
			userId: user._id
		},
		{
			title: 'Table',
			price: 11000,
			description: 'Some table',
			categoryId: furnitureCategory._id,
			image: 'table.jpeg',
			userId: user._id
		},
		{
			title: 'Macbook',
			price: 200000,
			description: 'Very cool Mac',
			categoryId: HiTechCategory._id,
			image: 'macbook.png',
			userId: root._id
		}
	);
	
	db.close();
});