const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Item = require('../models/Item');
const auth = require('../middleware/auth');
const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
	router.get('/', (req, res) => {
		const category = req.query.category;
		if(category) {
			Item.find({categoryId: category})
			.then(results => res.send(results))
			.catch(() => res.sendStatus(500));
		} else {
			Item.find().populate('categoryId')
			.then(results => res.send(results))
			.catch(() => res.sendStatus(500));
		}
	});
	
	router.get('/:id', (req, res) => {
		const id = req.params.id;
		Item.findOne({_id: id}).populate('userId', 'displayName phone _id').populate('categoryId')
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.post('/', [auth, upload.single('image')], (req, res) => {
		const itemData = req.body;
		
		if (req.file) {
			itemData.image = req.file.filename;
		} else {
			itemData.image = null;
		}
		itemData.userId = req.user._id;
		const item = new Item(itemData);
		
		item.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.delete('/:id', auth, (req, res) => {
		const id = req.params.id;
		Item.findOne({_id: id}).populate('userId').then(item => {
			if(req.user._id.equals(item.userId._id)) {
				Item.deleteOne({_id: id})
				.then(result => {
					if (result) res.send(result);
					else res.sendStatus(404);
				})
				.catch(() => res.sendStatus(500));
			} else {
				res.status(403).send({message: {error: 'You are not the master!'}});
			}
		});
	});
	
	return router;
};

module.exports = createRouter;